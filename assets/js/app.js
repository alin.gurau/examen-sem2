/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you require will output into a single css file (app.css in this case)
require("../css/app.scss");

// Need jQuery? Install it with "yarn add jquery", then uncomment to require it.
const $ = require("jquery");
require("datatables.net-bs4")($);

$(document).ready(function() {
  $("#myTable").DataTable({
    ajax: "http://examen-sem2.local/getStudents",
    columns: [{ data: "firstName" }, { data: "lastName" }, { data: "email" }]
  });
});

require("bootstrap");

console.log("Hello Webpack Encore! Edit me in assets/js/app.js");

import Vue from "vue";
import axios from "axios";
var jsapp = new Vue({
  el: "#js-app",
  delimiters: ["{*", "*}"],
  data: {
    students: []
  },
  methods: {},
  mounted: function() {
    axios
      .get("http://examen-sem2.local/getStudents")
      .then(response => (this.students = response));
  }
});
