<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use App\Entity\Students;

class StudentsController extends AbstractController
{
    /**
     * @Route("/students", name="students")
     */
    public function index()
    {
        return $this->render('students/index.html.twig', [
            'controller_name' => 'StudentsController',
        ]);
    }

    /**
     * @Route("/getStudents", name="getStudents")
     */
    public function getStudents()
    {

        $student = $this->getDoctrine()
            ->getRepository(Students::class)
            ->findAll();

        $serializer = $this->get('serializer');

        $response = $serializer->serialize($student, 'json');

        return new Response($response);
    }
}
